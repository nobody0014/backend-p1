package enum



type ServerType int

const (
	SOS    ServerType = 0
	RabbitMQ    ServerType = 1
)

func (server ServerType) String() string {
	servers := [...]string{
		"SOS",
		"RabbitMQ",}
	return servers[server]
}