module gifmaker

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/cavaliercoder/grab v2.0.0+incompatible // indirect
	github.com/go-cmd/cmd v1.0.3
	github.com/moul/http2curl v0.0.0-20170919181001-9ac6cf4d929b // indirect
	github.com/parnurzeal/gorequest v0.2.15
	github.com/pkg/errors v0.8.0 // indirect
	github.com/streadway/amqp v0.0.0-20180806233856-70e15c650864
	golang.org/x/net v0.0.0-20180926154720-4dfa2610cdf3 // indirect
)
