#!/bin/bash
die () {
    echo >&2 "$@"
    exit 1
}


[ $# -eq 3 ] || [ $# -eq 2 ] || die "not enough arguments" 
[ -f $1 ] || die "file does not exist"
echo $2 | grep -E -q '^.*\.gif$' || die "not .gif output file"

seconds=`ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$1"`
s1=`echo "scale=2; $seconds/(5)" | bc`
for i in `seq 2 4`;do
    name=$i-$1-%04d.png
    s2=`echo "scale=2; ($s1*$i)" | bc`
    ffmpeg -ss $s2 -t 2 -i $1 -vf scale=340:240 $name
done
convert *-$1-* $2
rm *-$1-*
