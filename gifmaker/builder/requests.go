package builder

import (
	. "gifmaker/model"
	"strconv"
	"bytes"
)



func ListObjectsInBucketRequest(host string, port int, bucketName string) Request {
	return Request{
		Host: host,
		Port: port,
		URL: "/" + bucketName,
		Method: "GET",
		Query: map[string]string{"list": ""},
		Headers: map[string]string{},
		Body: nil,
		//File: "",
	}
}


func DownloadObjectRequest(host string, port int, bucketName string, objectName string) Request {
	return Request{
		Host: host,
		Port: port,
		URL: "/" + bucketName + "/" + objectName,
		Method: "GET",
		Query: map[string]string{"": ""},
		Headers: map[string]string{},
		Body: nil,
		//File: "",
	}
}

func CreateObjectRequest(host string, port int, bucketName string, objectName string) Request {
	return Request{
		Host: host,
		Port: port,
		URL: "/" + bucketName + "/" + objectName,
		Method: "POST",
		Query: map[string]string{"create": ""},
		Headers: map[string]string{},
		Body: nil,
		//File: "",
	}
}

func UploadPartsObjectRequest(host string, port int, bucketName string, objectName string, partNumber int, md5 string, filesize string, body *bytes.Buffer) Request {
	return Request{
		Host: host,
		Port: port,
		URL: "/" + bucketName + "/" + objectName,
		Method: "PUT",
		Query: map[string]string{"partNumber": strconv.Itoa(partNumber)},
		Headers: map[string]string{"Content-Type": "","Content-MD5": md5, "Content-Length": filesize},
		Body: body,
	}
}

func CompleteUploadObjectRequest(host string, port int, bucketName string, objectName string) Request {
	return Request{
		Host: host,
		Port: port,
		URL: "/" + bucketName + "/" + objectName,
		Method: "POST",
		Query: map[string]string{"complete":""},
		Headers: map[string]string{},
		Body: nil,
		//File: "",
	}
}