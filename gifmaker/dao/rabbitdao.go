package dao

import (
	"errors"
	"gifmaker/enum"
	"github.com/streadway/amqp"
	"log"
	"time"
)


type RabbitDAO struct {
	Server string
	Port int
	Channel *amqp.Channel
	Queues map[string]amqp.Queue
	DefaultChannel string
}

// since we only need to send bucketname ever, this thing just have bucketname in it
func (r *RabbitDAO) Send(channelname string, bucketname string) error{
	if len(channelname) == 0 {
		channelname = r.DefaultChannel
	}
	err := r.Channel.Publish(
		"",     // exchange
		channelname, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing {
			ContentType: "text/plain",
			Body:        []byte(bucketname),
		})
	if err != nil {
		log.Println("Send failed")
		return err
	}
	return nil
}

//func (r *RabbitDAO) Read(channelname string) error {
//	if len(channelname) == 0  {
//		channelname = r.DefaultChannel
//	}
//	msgs, err := r.Channel.Consume(
//		channelname, // queue
//		"",     // consumer
//		true,   // auto-ack
//		false,  // exclusive
//		false,  // no-local
//		false,  // no-wait
//		nil,    // args
//	)
//	if err != nil {
//		return  errors.New("UnableToConsumeMsgs")
//	}
//	forever := make(chan bool)
//
//	go func() {
//		for d := range msgs {
//			//call functions to do it
//			log.Println(d)
//		}
//	}()
//	<-forever
//	return nil
//}

func (r *RabbitDAO) AddQueue(channelname string) error{
	if  _, exist := r.Queues[channelname]; exist {
		return errors.New("QueueExist")
	}
	if len(channelname) == 0 {
		 channelname = r.DefaultChannel
	}
	q, err := r.Channel.QueueDeclare(
		channelname, // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	if err != nil {
		log.Println("Add Queue Failed")
		return errors.New(err.Error())
	}
	r.Queues[channelname] = q
	return nil
}

func (r *RabbitDAO) Connect() error{
	log.Println(r.Server)
	log.Println(r.Port)
	var err error = nil
	var conn *amqp.Connection
	for counter := 0; counter < 10; counter++{
		conn, err = amqp.Dial("amqp://guest:guest@" + r.Server + "/")

		if err == nil {
			break
		}
		time.Sleep(5 * time.Second)
	}
	//rabbitmq
	//conn, err := amqp.Dial("rabbitmq")
	if err != nil {
		log.Println("Dailing Failed")
		return err
	}
	ch, err := conn.Channel()
	if err != nil {
		log.Println("Channel Failed")
		return err
	}
	log.Println("Assigning")
	r.Channel = ch
	r.DefaultChannel = enum.DEFAULT.String()
	r.Queues = map[string]amqp.Queue{}
	log.Println("Adding queue")
	err = r.AddQueue("")
	if err != nil {
		 return err
	}
	return nil
}