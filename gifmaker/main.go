package main

import (
	"encoding/json"
	. "gifmaker/builder"
	. "gifmaker/config"
	. "gifmaker/dao"
	"gifmaker/enum"
	. "gifmaker/model"
	. "gifmaker/utility"
	"github.com/go-cmd/cmd"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

/*
How gif maker works
1) Create client for reading from rabbit mq from
1.1) Create for reading rabbit mq is based on the config.toml file
1.2) Extract config.toml file from config struct in config.go
2) Once a msg comes from rabbit mq do this
2.1) Send a list checking from bucket -- see how many videos/files are there
2.2) take out all that are not valid format -- save the format in a config files or sth
2.3) execute make_thumbnail on all valid videos and output .gif behind
*/
/*
TODO:
Config file
Servers: SOS, RabbitMQ
Valid Video Format: anything that supported - have to check

Folder
1) config
Config.go --> decode servers, decode videos formats --> import no one
2) model
request --> import no one
3) builder
requests --> import only model
4) utility
utility --> keeping misc items, should be importing only model --> not anything else
5) dao
rabbitmqdao --> functions dedicate to connecting to rabbitmq [read etc]
sosdao --> functions dedicate to connecting to sos [making requests]


Functions
ListObjectsInBucket
--> list objects in buckets,

GetObjectInBucket
--> given a bucketname and an object name download object into a folder

Execute make_thumbnails
--> single function that take in path to video file and execute make thumb nail

Put Object in Bucket
--> ok we gonna need to do stuffs here
--> First we need to bucket name
--> then we need the object name [preferably the actual file name we made]
--> then we create multi ticket to that
--> then we upload that file onto the bucket
-->

*/

/*
main flow of main
create shit to read from rabbit mq
once something come, call something to make the main flow

*/
func main(){
	servers := ServerConfigs{}
	servers.Read()

	serverconfig,err := servers.GetConfig(enum.SOS.String())
	if err != nil {
		log.Fatal("UnableToGetConfig")
	}

	rabbitconfig,err := servers.GetConfig(enum.RabbitMQ.String())
	if err != nil {
		log.Fatal("UnableToGetConfig")
	}
	env, f := os.LookupEnv("PROD")
	var sosserver string
	var sosport int
	var rabbitserver string
	var rabbitport int

	log.Println(env,f)
	if f && env == "DOCKER" {
		sosserver = serverconfig.ProductionServer
		sosport = serverconfig.ProductionPort
		rabbitserver = rabbitconfig.ProductionServer
		rabbitport = rabbitconfig.ProductionPort
	}else{
		sosserver = serverconfig.DevServer
		sosport = serverconfig.DevPort
		rabbitserver = rabbitconfig.DevServer
		rabbitport = rabbitconfig.DevPort
	}



	rabbitmq := RabbitDAO{}
	rabbitmq.Server = rabbitserver
	rabbitmq.Port = rabbitport
	err = rabbitmq.Connect()
	if err != nil {
		log.Println("RabbitMQFAILED")
		log.Fatal(err.Error())
	}
	//rabbitmq.Read("") //meaning use default
	// the rest will continue in rabbitdao package
	channelname := rabbitmq.DefaultChannel
	msgs, err := rabbitmq.Channel.Consume(
		channelname, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	if err != nil {
		log.Println(err.Error())
		log.Fatal(err.Error())
	}
	forever := make(chan bool)

	go func() {
		// MAIN LOOP
		for d := range msgs {
			//call functions to do it
			//basically do whatever hell you want here
			//steps
			//send bucketname to sos to do listing
			//execute the command, if exit != 0, it's not a good extension
			var job Job
			json.Unmarshal([]byte(d.Body), &job)
			//unpack the object
			bucketname := job.BucketName
			filename := job.ObjectName
			outputfile := job.OutputName




			//execute the main flow of downloading
			//1) make folder for that bucket
			//2) download object into that bucket
			//3) execute command [if exit code != 0, then it failed]
			//input file here
			//output file here
			//once output execution is done, delete the input file and continue
			downloadreq := DownloadObjectRequest(sosserver,sosport,bucketname,filename)
			err := ExecuteDownloadRequest(downloadreq,filename)
			if err != nil {
				//since we didnt check it might be dead
				log.Println("DOWNLOADREQ")
				log.Println(err.Error())
				continue
			}
			//write store the file here


			stat,err := os.Stat(filename)
			if err != nil {
				log.Println(err.Error())
				DeleteFile(filename)
				continue
			}
			log.Println(stat.Size())

			//execute

			makeThumbnailCmd := cmd.NewCmd("make_thumbnail.sh",filename,outputfile)
			//makeThumbnailCmd.Args = []string {filename, outputfile}
			statusChan := makeThumbnailCmd.Start() // non-blocking

			// Stop command after 10 minutes
			go func() {
				<-time.After(10 * time.Minute)
				makeThumbnailCmd.Stop()
			}()

			// Check if command is done
			select {
			case finalStatus := <-statusChan:
				// done
				if finalStatus.Complete && finalStatus.Error != nil{
					log.Println(finalStatus.Error.Error())
					err = DeleteFile(filename)
					if err != nil {
						log.Println(err.Error())
					}
					continue
				}
			default:
				log.Println("Still Running")
			}

			// Block waiting for command to exit, be stopped, or be killed
			finalStatus := <-statusChan

			if finalStatus.Exit > 0 {
				//log.Println(finalStatus)
				err = DeleteFile(filename)
				if err != nil {
					log.Println(err.Error())
				}
				err = DeleteFile(outputfile)
				if err != nil {
					log.Println(err.Error())
				}
				continue
			}


			//send out delete request on the outputfile in the SOS
			client := &http.Client{}
			// Create request
			deleteurl := "http://" + sosserver + ":" + strconv.Itoa(sosport) + "/" + bucketname + "/" + outputfile + "?delete"
			req, err := http.NewRequest("DELETE", deleteurl, nil)
			if err != nil {
				log.Println(err)
				return
			}
			// Fetch Request
			resp, err := client.Do(req)
			if err != nil {
				log.Println(err)
				err = DeleteFile(filename)
				if err != nil {
					log.Println(err.Error())
				}
				err = DeleteFile(outputfile)
				if err != nil {
					log.Println(err.Error())
				}
				continue
			}else {
				// Read Response Body
				_, err = ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println(err)
					err = DeleteFile(filename)
					if err != nil {
						log.Println(err.Error())
					}
					err = DeleteFile(outputfile)
					if err != nil {
						log.Println(err.Error())
					}
					continue
				}
				defer resp.Body.Close()
			}


			// now we upload the gif
			// first we create the ticket
			objcreatereq := CreateObjectRequest(sosserver,sosport,bucketname,outputfile)
			_, body, errs := ExecuteRequest(objcreatereq)
			if errs != nil && len(errs) > 0 {
				printOutErrors(errs)
				log.Println(body)
				err = DeleteFile(filename)
				if err != nil {
					log.Println(err.Error())
				}
				err = DeleteFile(outputfile)
				if err != nil {
					log.Println(err.Error())
				}
				// we may actually need to further check which kind of error it is but
				// i dont think that's the worker's job
				continue
			}

			////2nd
			////now we put the actual file up
			//stat, err = os.Stat(outputfile)
			//if err != nil {
			//	log.Println("Stat")
			//	log.Fatal(err.Error())
			//	continue
			//}
			//of, err := os.Open(outputfile)
			//if err != nil {
			//	log.Println(err.Error())
			//	err = DeleteFile(filename)
			//	if err != nil {
			//		log.Println(err.Error())
			//	}
			//	continue
			//}
			//md5str,err := HashFileMD5(outputfile)
			//if err != nil {
			//	log.Println(err.Error())
			//	err = DeleteFile(filename)
			//	if err != nil {
			//		log.Println(err.Error())
			//	}
			//	continue
			//}
			//filesize  := string(strconv.AppendInt([]byte{},stat.Size(),10))
			//reqbody := &bytes.Buffer{}
			//_, err = io.Copy(reqbody, of)
			//if err != nil {
			//	log.Println(err.Error())
			//	continue
			//}
			//of.Close()

			//uploadobjectreq := UploadPartsObjectRequest(sosserver,sosport,bucketname,outputfile,1,md5str,filesize,reqbody)
			_, body, errs = ExecuteFileUploadRequest(sosserver,sosport,bucketname, outputfile)

			if errs != nil && len(errs) > 0{
				printOutErrors(errs)
				log.Println(body)

				err = DeleteFile(filename)
				if err != nil {
					log.Println(err.Error())
				}
				err = DeleteFile(outputfile)
				if err != nil {
					log.Println(err.Error())
				}
				continue
			}

			//3rd we complete the upload process
			completeobjectreq := CompleteUploadObjectRequest(sosserver,sosport,bucketname,outputfile)
			_, body, errs = ExecuteRequest(completeobjectreq)
			if errs != nil && len(errs) > 0{
				printOutErrors(errs)
				log.Println(body)
				err = DeleteFile(filename)
				if err != nil {
					log.Println(err.Error())
				}
				err = DeleteFile(outputfile)
				if err != nil {
					log.Println(err.Error())
				}
				continue
			}


			err = DeleteFile(filename)
			if err != nil {
				log.Println(err.Error())
			}
			err = DeleteFile(outputfile)
			if err != nil {
				log.Println(err.Error())
			}


			log.Println(bucketname)
		}
	}()
	<-forever
}

func printOutErrors(errs []error) {
	for i := range errs  {
		log.Println(errs[i].Error())
	}
}