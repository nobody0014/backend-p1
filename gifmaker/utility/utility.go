package utility

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	. "gifmaker/model"
	"github.com/cavaliercoder/grab"
	"github.com/parnurzeal/gorequest"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

func getRequest(request Request) (*gorequest.SuperAgent, error){
	rawurl := "http://" + request.Host + ":" + strconv.Itoa(request.Port) + request.URL
	if request.Method == "GET" {
		return 	gorequest.New().Get(rawurl),nil
	}else if request.Method == "PUT" {
		return gorequest.New().Put(rawurl),nil
	}else if request.Method == "DELETE" {
		return gorequest.New().Delete(rawurl),nil
	}else if request.Method == "POST" {
		return gorequest.New().Post(rawurl),nil
	}else {
		return nil,errors.New("InvalidMethod")
	}
}

func ExecuteRequest(request Request) (gorequest.Response, string, []error) {
	req, err := getRequest(request)
	if err != nil {
		errs := [] error {}
		errs = append(errs, err)
		return nil,"",errs
	}
	req.Timeout(10 * time.Minute)
	for key,value := range request.Headers {
		req.Header[key] = value
	}
	for key,value := range request.Query {
		req.QueryData.Set(key,value)

	}
	if request.Body != nil {
		log.Println("in dat",request.Body.Len())
		req.SendFile(request.Body.Bytes())
	}
	log.Println("sending req")
	resp, body, errs := req.End()
	log.Println("done req")
	if errs != nil {
		return resp, body, errs
	}
	if resp.StatusCode != 200 {
		errs := append(errs, errors.New(body))
		return resp,body,errs
	}
	fmt.Println(resp.StatusCode)
	return resp,body,nil
}

func ExecuteFileUploadRequest(host string, port int, bucketname string, outputname string) (*http.Response, string, []error) {
	rawurl := "http://" + host + ":" + strconv.Itoa(port) + "/" + bucketname + "/" + outputname + "?partNumber=1"

	bodyBuf := &bytes.Buffer{}
	// open file handle
	fh, err := os.Open(outputname)
	if err != nil {
		fmt.Println("error opening file")
		return nil,"",append([]error{},err)
	}
	fs,err := fh.Stat()
	if err != nil {
		return nil,"",append([]error{},err)
	}
	defer fh.Close()
	//iocopy
	_, err = io.Copy(bodyBuf, fh)
	if err != nil {
		return nil,"",append([]error{},err)
	}

	md5h,err := HashFileMD5(outputname)
	if err != nil {
		return nil,"",append([]error{},err)
	}


	fsize := string(strconv.AppendInt([]byte{},fs.Size(),10))
	//log.Println(len(bodyBuf.Bytes()), fsize, outputname)
	req, err := http.NewRequest(http.MethodPut,  rawurl, bodyBuf)
	if err != nil {
		return nil,"", append([]error{},err)
	}
	req.Header.Add("Content-MD5", md5h)
	req.Header.Set("Transfer-Encoding", "chunked")
	req.Header.Add("Content-Type", "image/gif")
	req.Header.Add("Content-Length", fsize)

	hc := &http.Client{}
	log.Println(rawurl)
	log.Println(req.Header.Get("Content-Length"), len(bodyBuf.Bytes()), fsize, outputname,req.Header.Get("Content-MD5"),req.Header.Get("Content-Type"))
	resp,err := hc.Do(req)
	log.Println(req.Header.Get("Content-Length"), len(bodyBuf.Bytes()), fsize, outputname,req.Header.Get("Content-MD5"),req.Header.Get("Content-Type"))
	//httputil.DumpRequestOut()
	if err != nil {
		return resp,"", append([]error{},err)
	}
	return resp,"",nil
}

func ExecuteDownloadRequest(request Request, outputfile string)   error {
	url := "http://" + request.Host + ":" + strconv.Itoa(request.Port) + request.URL
	res, err := grab.Get(outputfile,url)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	if res.HTTPResponse.StatusCode != 200 {
		log.Println( res.HTTPResponse.StatusCode )
		return  errors.New("BadStatusCode")
	}
	//err = CreateFile(outputfile)
	//if err != nil {
	//	return err
	//}
	//f, err := os.Open(outputfile)
	//if err != nil {
	//	return err
	//}
	//io.Copy(f, res.Body)
	//defer f.Close()
	//defer res.Body.Close()
	return nil
}

func ExecuteMakeThumbnail(job MakeThumbnailJob) error{

	return nil
}


func CreateFile(filepath string) error{
	_, err := os.Stat(filepath)
	// create file if not exists
	if !os.IsNotExist(err) {
		log.Print("Deleting file")
		err = DeleteFile(filepath)
		if err !=nil {
			log.Print("Error: "+err.Error())
			return err
		}
	}// no else cus u just create the file you idiot
	log.Print("Creating file")
	file, err := os.Create(filepath)
	if err != nil {
		log.Print("Error: "+err.Error())
		return err
	}
	file.Close()
	return nil
}

func DeleteFile(filepath string) error {
	// delete file
	err := os.Remove(filepath)
	if err != nil {
		log.Print("Error: "+err.Error())
		return err
	}
	return nil
}


func HashFileMD5(filePath string) (string, error) {
	var returnMD5String string
	file, err := os.Open(filePath)
	if err != nil {
		log.Print("Error: "+err.Error())
		return returnMD5String, err
	}
	defer file.Close()
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		log.Print("Error: "+err.Error())
		return returnMD5String, err
	}
	hashInBytes := hash.Sum(nil)[:16]
	returnMD5String = hex.EncodeToString(hashInBytes)
	return returnMD5String, nil
}