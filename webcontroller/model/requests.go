package model

import (
	"bytes"
)

type Request struct {
	Host string
	Port int
	URL string
	Method string
	Query map[string]string
	Headers map[string]string
	Body *bytes.Buffer
}



