package model




type Job struct {
	BucketName string `json:"bucketName"`
	ObjectName string `json:"objectName"`
	OutputName string `json:"outputName"`
}
