package model


type ListObjectsInBucketResponse struct {
	Created   int64 `json:"created"`
	Modified int64 `json:"modified"`
	Name string `json:"name"`
	Objects []ReturnObject `json:"objects"`
}

type ReturnObject struct {
	Name string `json:"name"`
	ETag string `json:"eTag"`
	Created int64 `json:"created"`
	Modified int64 `json:"modified"`
}
