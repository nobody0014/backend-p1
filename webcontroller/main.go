package main

import (
	. "webcontroller/endpoint"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
	. "webcontroller/config"
	. "webcontroller/enum"
)



func main(){
	configs := ServerConfigs{}
	configs.Read()
	webcontrollerconfig,_ := configs.GetConfig(WebController.String())
	env, f := os.LookupEnv("PROD")
	var host string
	var port int

	if f && env == "DOCKER" {
		host = webcontrollerconfig.ProductionServer
		port = webcontrollerconfig.ProductionPort
	}else{
		host = webcontrollerconfig.DevServer
		port = webcontrollerconfig.DevPort
	}
	allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With"})
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})


	router := mux.NewRouter()
	router.HandleFunc("/gif", ListAllFilesEndpoints).
		Queries("bucketName","{bucketName}","list","{list}").
		Methods("GET")
	router.HandleFunc("/vid", ListAllFilesEndpoints).
		Queries("bucketName","{bucketName}","list","{list}").
		Methods("GET")
	router.HandleFunc("/job", JobEndpoints).
		Queries("bucketName","{bucketName}","create","{create}").
		Methods("POST")
	r2 := handlers.CORS(allowedHeaders,allowedOrigins,allowedMethods) (router)
	srv := &http.Server{
		Handler:      handlers.LoggingHandler(os.Stdout, r2),
		Addr:         host + ":" + strconv.Itoa(port),
		WriteTimeout: 300 * time.Second,
		ReadTimeout:  300 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}