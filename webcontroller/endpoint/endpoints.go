package endpoint

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	. "webcontroller/builder"
	. "webcontroller/config"
	. "webcontroller/dao"
	"webcontroller/enum"
	. "webcontroller/enum"
	. "webcontroller/model"
	. "webcontroller/utility"
)

/*
3 APIS:
1) API that submits a job to the queue
2) API that, given a bucket, creates multiple jobs for all videos in the bucket and submits them to the queue
3) API that lists all GIF images in a user-specified bucket
*/

var configs  = ServerConfigs{}
var rabbitdao =  RabbitDAO {}
var sosdao = SOSDAO {}
var gifpat = "[-_a-zA-Z0-9]+[-._a-zA-Z0-9]*.gif$"
var vidpat = "[-_a-zA-Z0-9]+[-._a-zA-Z0-9]*.(mov|mp4|avi|mkv)$"


func init(){
	configs.Read()
	rabbitconfig, _ := configs.GetConfig(RabbitMQ.String())
	env, f := os.LookupEnv("PROD")
	var host string
	var port int

	sosconfig, err := configs.GetConfig(SOS.String())
	if err != nil {
		log.Fatal("SOSDEAD")
	}

	if f && env == "DOCKER" {
		host = rabbitconfig.ProductionServer
		port = rabbitconfig.ProductionPort
		sosdao.Server = sosconfig.ProductionServer
		sosdao.Port = sosconfig.ProductionPort
	}else{
		host = rabbitconfig.DevServer
		port = rabbitconfig.DevPort
		sosdao.Server = sosconfig.ProductionServer
		sosdao.Port = sosconfig.ProductionPort
	}


	rabbitdao.Server = host
	rabbitdao.Port = port
	rabbitdao.DefaultChannel = DEFAULT.String()
	rabbitdao.Connect()
}


func JobEndpoints(w http.ResponseWriter, r *http.Request){
	bucketName := r.URL.Query().Get("bucketName")
	outputName := r.URL.Query().Get("outputName")
	objectName := r.URL.Query().Get("objectName")
	bucketep := false

	if len(bucketName) == 0 {
		http.Error(w, "MissingBucketName", http.StatusBadRequest)
		return
	}else{
		bucketep = true
	}

	if len(objectName) == 0 && len(outputName) == 0 && bucketep{
		CreateAndSubmitJobsForBucketEndpoint(w,r)
		return
	}else if len(objectName) != 0 && bucketep{
		CreateAndSubmitAJobForBucketEndpoint(w,r)
		return
	}else {
		http.Error(w, "MissingObjectName|MissingOutputName", http.StatusBadRequest)
		return
	}
}

func CreateAndSubmitAJobForBucketEndpoint(w http.ResponseWriter, r *http.Request) {
	bucketName := r.URL.Query().Get("bucketName")
	objectName := r.URL.Query().Get("objectName")
	outputName := r.URL.Query().Get("outputName")
	if len(outputName) == 0 {
		outputName = objectName + ".gif"
	}
	if len(objectName) != 0 {
		match, err := regexp.MatchString(vidpat, objectName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if !match {
			http.Error(w, "InvalidVideoFormat", http.StatusBadRequest)
			return
		}
	}
	resp, err := http.Head("http://" + sosdao.Server + ":" + strconv.Itoa(sosdao.Port) + "/" + bucketName + "/" + objectName)
	if resp.StatusCode != http.StatusOK {
		http.Error(w, "NoBucket|NoObject", http.StatusBadRequest)
		return
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	job := Job{BucketName:bucketName, ObjectName:objectName, OutputName:outputName}
	err = rabbitdao.Send("", job)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}


/*
Create a map to send stuffs to the rabbit mq
*/
func CreateAndSubmitJobsForBucketEndpoint(w http.ResponseWriter, r *http.Request){
	//since path is /job --> no need to check for it
	bucketName := r.URL.Query().Get("bucketName")
	request := ListObjectsInBucketRequest(sosdao.Server,sosdao.Port,bucketName)
	_,body, errors := ExecuteRequest(request)
	if errors != nil && len(errors) > 0 {
		http.Error(w, turnErrorsToString(errors), http.StatusBadRequest)
		return
	}
	var listresponse ListObjectsInBucketResponse
	err := json.Unmarshal([]byte(body),&listresponse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if len(body) == 0 {
		http.Error(w, "INVALIDLISTING", http.StatusBadRequest)
		return
	}

	// send all objects
	jobsent := 0
	for _,obj := range listresponse.Objects {
		njob := Job{BucketName:bucketName,ObjectName:obj.Name,OutputName:obj.Name+".gif"}
		match, err := regexp.MatchString(vidpat, njob.ObjectName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if !match {
			continue
		}
		err = rabbitdao.Send("", njob)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		jobsent += 1
	}
	if jobsent == 0 {
		http.Error(w, "No Video Found In The Bucket", http.StatusBadRequest)
		return
	}
}


/*
Video Formats: mov avi and mp4
This endpoint is literally same as list gif = maybe we can change it later to make it take extension instead
*/
func ListAllFilesEndpoints(w http.ResponseWriter, r *http.Request){
	p := strings.Split(r.URL.Path,"/")[1]
	if p == "gif" {
		ListAllFilesWithGivenExtensionsEndpoint(w,r,gifpat)
	}else if p == "vid" {
		ListAllFilesWithGivenExtensionsEndpoint(w,r,vidpat)
	}else{
		http.Error(w, "InvalidPath", http.StatusNotFound)
		return
	}
}


func ListAllFilesWithGivenExtensionsEndpoint(w http.ResponseWriter, r *http.Request, regpat string){
	//since path is /job --> no need to check for it
	bucketName := r.URL.Query().Get("bucketName")
	if len(bucketName) == 0 {
		http.Error(w, "MissingBucketName", http.StatusBadRequest)
		return
	}
	//get SOS server config
	sos := enum.SOS
	//do a check to SOS to see if bucket is valid by doing a listing
	//also by listing we get what we want back if there's no problem
	sosserver, err := configs.GetConfig(sos.String())
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	request := ListObjectsInBucketRequest(sosserver.DevServer,sosserver.DevPort,bucketName)
	_,body, errors := ExecuteRequest(request)
	if errors != nil && len(errors) > 0 {
		log.Println(turnErrorsToString(errors))
		http.Error(w, turnErrorsToString(errors), http.StatusBadRequest)
		return
	}
	//list the body, the items in the bucket
	//probably filter out so that it only left gif
	//for now just return that body
	//for better management, only return gif items
	var objects ListObjectsInBucketResponse
	err = json.Unmarshal([]byte(body), &objects)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	returnobjects := []ReturnObject{}
	for _,obj := range objects.Objects {
		match, err := regexp.MatchString(regpat, obj.Name)
		if err != nil {
			log.Println(err.Error())
			continue
		}
		if match {
			returnobjects = append(returnobjects, obj)
		}
	}
	w.Header().Set("Content-Type", "application/json")
	rstr, err := json.Marshal(returnobjects)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Write(rstr)
}


func turnErrorsToString(errs []error) string{
	err := ""
	for _,e := range errs {
		log.Println(e.Error())
		err = err + e.Error() + "\n"
	}
	return err
}