package enum

type ServerType int

const (
	SOS    ServerType = 0
	RabbitMQ    ServerType = 1
	WebController    ServerType = 2
)

func (server ServerType) String() string {
	servers := [...]string{
		"SOS",
		"RabbitMQ",
		"WebController",}

	return servers[server]
}