package builder

import (
	. "webcontroller/model"
)



func ListObjectsInBucketRequest(host string, port int, bucketName string) Request {
	return Request{
		Host: host,
		Port: port,
		URL: "/" + bucketName,
		Method: "GET",
		Query: map[string]string{"list": ""},
		Headers: map[string]string{},
		Body: nil,
	}
}
