package utility

import (
	"errors"
	"fmt"
	"github.com/parnurzeal/gorequest"
	"strconv"
	. "webcontroller/model"
)


func getRequest(request Request, requestCreator *gorequest.SuperAgent) (*gorequest.SuperAgent, error){
	rawurl := "http://" + request.Host + ":" + strconv.Itoa(request.Port) + request.URL
	if request.Method == "GET" {
		return 	requestCreator.Get(rawurl),nil
	}else if request.Method == "PUT" {
		return requestCreator.Put(rawurl),nil
	}else if request.Method == "DELETE" {
		return requestCreator.Delete(rawurl),nil
	}else if request.Method == "POST" {
		return requestCreator.Post(rawurl),nil
	}else {
		return nil,errors.New("InvalidMethod")
	}
}

func ExecuteRequest(request Request) (gorequest.Response, string, []error) {

	r := gorequest.New()
	req, err := getRequest(request, r)
	if err != nil {
		errs := [] error {}
		errs = append(errs, err)
		return nil,"",errs
	}
	for key,value := range request.Headers {
		req.Header[key] = value
	}
	for key,value := range request.Query {
		req.QueryData.Set(key,value)
	}
	if request.Body != nil {
		req.Send(request.Body)
	}
	resp, body, errs := req.End()

	if errs != nil {
		return resp, body, errs
	}
	fmt.Println(resp.StatusCode)
	return resp,body,nil
}