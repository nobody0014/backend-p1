package config


import (
	"errors"
	"log"

	"github.com/BurntSushi/toml"
)

// Represents database server and credentials
type ServerConfig struct {
	Type string
	ProductionServer string
	ProductionPort int
	DevServer string
	DevPort int
}

type ServerConfigs struct {
	Servers map[string]ServerConfig
}


// Read and parse the configuration file
func (c *ServerConfigs) Read() {
	if _, err := toml.DecodeFile("config.toml", &c); err != nil {
		log.Fatal(err)
	}
}

// Get config from server config
func (c *ServerConfigs) GetConfig(configname string) (ServerConfig,error){
	for _,conf := range c.Servers  {
		if conf.Type == configname {
			return conf,nil
		}
	}
	return ServerConfig{},errors.New("ConfigNotFound")
}